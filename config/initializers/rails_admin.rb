
RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0
  config.authorize_with do
    authenticate_or_request_with_http_basic('Login required') do |username, password|
      # user = User.where(email: username, password: password, admin: true).first
      # user
      if username == "descolga2" && password == "10Mayo2008"
        true
      end
    end
  end
  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  #config.show_gravatar true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
  config.model Post do
      include_all_fields
      field :content, :ck_editor
  end
  config.model Category do
    include_all_fields
    field :images do
      visible false
    end
    field :posts do
      visible false
    end
  end
end
