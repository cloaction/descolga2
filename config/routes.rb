Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'nosotros', to: 'about#index', as: :nosotros
  get 'servicios', to: 'services#index', as: :servicios
  get 'trabajos', to: 'jobs#index', as: :trabajos
  get 'contacto', to: 'contact#index', as: :contacto
  post 'contacto', to: 'contact#create'

  get 'galerias/:slug', to: 'category#show', as: :galeria
  get 'galerias', to: 'category#index', as: :galerias


  root 'home#index'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
