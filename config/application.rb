require_relative 'boot'

require 'rails/all'
require 'i18n'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
ENV['RAILS_ADMIN_THEME'] = 'rollincode'

Bundler.require(*Rails.groups)

module Descolga2
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.i18n.enforce_available_locales = true
    config.i18n.default_locale = :es
    config.web_console.whiny_requests = false
    config.assets.enabled = true
    config.assets.paths << Rails.root.join("app", "assets", "fonts")
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      address: "descolga2.com",
      port: 465,
      user_name: "info@descolga2.com",
      password: "10Mayo2008",
      authentication: :login,
      enable_starttls_auto: true,
      openssl_verify_model: 'none'
    }

    config.action_mailer.default_url_options = {
      host: "descolga2.com"
    }
  end
end
