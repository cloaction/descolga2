class CategoryController < ApplicationController
  def index
    @categories = Category.all
  end

  def show
    redirect_to galerias_path unless params[:slug].present?
    @category = Category.find_by(slug: params[:slug])
  end
end
