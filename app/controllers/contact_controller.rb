class ContactController < ApplicationController
  def index
    @contact = Contact.new
  end
  def create
    @contact = Contact.new(contact_params)

    if @contact.valid?
      send = ContactMailer.new_contact(@contact).deliver
      if send
        redirect_to contacto_path, notice: "Mensaje Enviado!"
      else
        flash[:alert] = "Ups, no se ha enviado!"
      end
    else
      flash[:alert] = "Ups, te has dejado algo!"
      render :index
    end
  end

private

  def contact_params
    params.require(:contact).permit(:name, :telefono, :email, :content)
  end
end
