class Contact

  include ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :name, :telefono, :email, :content

  validates :name,
    presence: true

  validates :telefono,
    presence: true
    
  validates :email,
    presence: true

  validates :content,
    presence: true

end
