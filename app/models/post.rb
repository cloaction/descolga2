# == Schema Information
#
# Table name: posts
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  slug        :string(255)
#  content     :text(65535)
#  status      :integer
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Post < ApplicationRecord
  belongs_to :category

  def status_enum
     [['Visible', 1],['Borrador',0]]
  end
  scope :visibles, -> (){
    where(status: 1).order(created_at: :desc)
  }

end
