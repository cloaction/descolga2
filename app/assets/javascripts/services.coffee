$(document).ready ->
  $(window).scroll ->
    if $(this).scrollTop() > 100
      $('.scrollToTop').fadeIn()
    else
      $('.scrollToTop').fadeOut()
    return
  #Click event to scroll to top
  $('.scrollToTop').click ->
    $('html, body').animate { scrollTop: 0 }, 800
    false
  return
