class ContactMailer < ApplicationMailer
  default from: 'info@descolga2.com'
  default to: 'info@descolga2.com'
  def new_contact(contact)
    @contact = contact
    mail(subject: '[DESCOLGA2] Nuevo contacto quiere que le llames')
  end
end
